require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT;
const hbs = require('hbs');

// TODO requiere('hbs')
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');

//* servir contenido statico
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.render('home', {
    name: 'Vacaciones online',
  });
});

app.get('/generic', (req, res) => {
  res.render('generic', {
    name: 'Vacaciones online',
  });
});

app.get('/elements', (req, res) => {
  res.render('elements', {
    name: 'Vacaciones online',
  });
  // res.sendFile(__dirname + '/public/elements.hbs');
});

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/public/404.html');
});

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
